# Example case for the simulation of membrane separation processes in hollow fibers

Implementation of non-planar membrane boundary conditions. The presense of neighboring membranes is simulated through symmetry and periodic boundaries.

Please cite this article if you're using these boundary conditions:
Aschmoneit, F., Hélix-Nielsen, C. Computational Fluid Dynamics of Forward Osmosis Processes in: Basile, A., Cassano, A., Rastogi, N. ed. Current Trends and Future Developments on (Bio-) Membranes: Recent Advances on Reverse and Forward Osmosis, Elsevier, 2019, pp. 81-111.
